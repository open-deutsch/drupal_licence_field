<?php

namespace Drupal\od_licence_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\Entity\User;
use Drupal\od_licence_field\CreativeCommons;

/**
 * A formatter for licence fields.
 *
 * @FieldFormatter(
 *   id = "od_licence_formatter",
 *   label = @Translation("Open Deutsch Licence Formatter"),
 *   description = @Translation("This formatter provides output of a licence item on the entity."),
 *   field_types = {
 *     "od_licence_field",
 *   }
 * )
 *
 * @author h2b
 */
class LicenceFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements (FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $licenceLink = CreativeCommons::linkMap()[$item->licence];
      $elements[$delta] = [
          'licence' => [
              '#markup' =>
                  $this->t('<small>Licence:') . ' ' . $licenceLink . ' ' .
                  $this->t('@origin (according to @uid at @time)</small>', [
                      '@origin' => $this->originator($item),
                      '@uid' => User::load($item->uid)->getDisplayName(),
                      '@time' => date('Y-m-d H:i:s' , $item->created),
                  ]),
          ],
      ];
    }
    return $elements;
  }

  private function originator ($item) {
    $origin = json_decode($item->origin, true);
    if ($origin=== NULL) return '';
    $originator = '';
    if ($origin['single_originator']['check'] == 1) {
      $originator .= User::load($item->uid)->getDisplayName();
    } elseif ($origin['multiple_originators']['check'] == 1) {
      if (isset($origin['multiple_originators']['group']))
        $originator .= $origin['multiple_originators']['group'];
    } elseif ($origin['other_originator']['check'] == 1) {
      if (isset($origin['other_originator']['originator']))
        $originator .= $origin['other_originator']['originator'];
    }
    return strlen($originator)>0 ?
        $this->t('by @originator', [ '@originator' => $originator]) :
        '';
  }

}
