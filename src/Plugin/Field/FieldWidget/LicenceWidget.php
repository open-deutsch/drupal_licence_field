<?php

namespace Drupal\od_licence_field\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\od_licence_field\CreativeCommons;

/**
 * A widget for licence fields.
 *
 * @FieldWidget(
 *   id = "od_licence_widget",
 *   label = @Translation("Open Deutsch Licence Widget"),
 *   description = @Translation("This widget provides input of a licence item on the entity."),
 *   field_types = {
 *     "od_licence_field",
 *   },
 * )
 *
 * @author h2b
 */
class LicenceWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement (FieldItemListInterface $items, $delta, array $element,
      array &$form, FormStateInterface $form_state) {
    //Implementation hint: Do not retrieve default values on required checkboxes.
    //Always set them to false instead. Otherwise we get legal issues and also
    //problems with the overall field default value when adding to a type.
    $element += [
        '#type' => 'fieldset',
        '#title' => t('Licence Information'),
        '#element_validate' => [
            [$this, 'validate'],
        ],
    ];
    $element['info'] = [
        '#type' => 'item',
        '#title' => $this->t('Origin'),
        '#description' => $this->t('What is the origin of this matter? ' .
            'Click on the right answer and fill in the form. ' .
            '<strong><em>You must choose exactly one of these originator clauses.</em></strong>'),
    ];
    $element['origin'] = [
        '#type' => 'container',
    ];
    $this->addSingleForm($element['origin'], $items[$delta]);
    $this->addGroupForm($element['origin'], $items[$delta]);
    $this->addOtherForm($element['origin'], $items[$delta]);
    $element['licence'] = [
        '#type' => 'radios',
        '#title' => $this->t('Licence'),
        '#description' => $this->t('Select a licence from the list.'),
        '#default_value' => $this->getOrElse($items[$delta]->licence, null),
        '#options' => CreativeCommons::htmlMap(),
        '#required' => true,
    ];
    $element['source_url'] = [
        '#type' => 'url',
        '#title' => $this->t('Source URL'),
        '#description' => $this->t('If you have a link to the original source of this matter, ' .
            'enter it here. This must be an external URL such as http://example.com.'),
        '#default_value' =>
            Html::escape(UrlHelper::filterBadProtocol($this->getOrElse($items[$delta]->source_url, null))),
    ];
    $element['additional_notes'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Additional Licence Notes'),
        '#description' => $this->t('If there is anything else to say regarding the licence of this matter, enter it here.'),
        '#default_value' => Html::escape($this->getOrElse($items[$delta]->additional_notes, null)),
    ];
    $element['privacy_statement'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Statement'),
        '#description' => $this->t('Dieses von mir hier veröffentlichte Werk enthält ' .
            'keine Bilder oder personenbezogene Daten von Menschen oder ich habe deren ' .
            'Einverständnis eingeholt und kann es im Zweifelsfall nachweisen oder ' .
            'es liegt ein zeitgeschichtliches oder allgemeines öffentliches Interesse vor.'),
        '#default_value' => false,
        '#required' => true,
    ];
    $element['uid'] = [
        '#type' => 'value',
        '#value' => \Drupal::currentUser()->id(),
    ];
    $element['created'] = [
        '#type' => 'value',
        '#value' => \Drupal::time()->getCurrentTime(),
    ];
    return $element;
  }

  /**
   * Validates that exactly one originator statement is checked.
   * @param array $element
   * @param FormStateInterface $form_state
   */
  public function validate (array $element, FormStateInterface $form_state) {
    $check0 = $element['origin']['single_originator']['check'];
    $check1 = $element['origin']['multiple_originators']['check'];
    $check2 = $element['origin']['other_originator']['check'];
    $sum = $this->count($check0['#value']) + $this->count($check1['#value']) +
        $this->count($check2['#value']);
    if ($sum != 1) {
      $message = $this->t('You must choose exactly one originator clause.');
      $form_state->setError($check0, $message);
      $form_state->setError($check1, $message);
      $form_state->setError($check2, $message);
    }
  }

  private function count ($check) {
    return empty($check) ? 0 : 1;
  }

  private function addSingleForm (array &$container, FieldItemInterface $item) {
    $name = 'single_originator';
    $this->addFieldset($container, $name, $this->t('I am the single originator'), 0);
    $container[$name]['check'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Statement'),
        '#description' => $this->t('Ich versichere, dass ich die/der alleinige Urhebende ' .
            'dieses Werkes bin. Ich wähle für die Veröffentlichung die folgende Lizenz.'),
        '#default_value' => false,
    ];
  }

  private function decodeOrElse ($prop, $key1, $key2, $default) {
    $json = $this->getOrElse($prop, null);
    $decode = json_decode($json, true);
    if ($decode === NULL) return $default;
    $result = $this->getOrElse($decode[$key1][$key2], $default);
    return $result;
  }

  private function getOrElse ($value, $default) {
    return isset($value) ? $value : $default;
  }

  private function addFieldset (array &$container, string $name, string $title, int $origin) {
    $container[$name] = [
        '#type' => 'details',
        '#title' => $title,
        '#states' => [
            'visible' => [
                ':input[name="origin"]' => ['value' => $origin]
            ]
        ]
    ];
  }

  private function addGroupForm (array &$container, FieldItemInterface $item) {
    $name = 'multiple_originators';
    $this->addFieldset($container, $name, $this->t('I am one of several people who ' .
        'are the originators'), 1);
    $container[$name]['group'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Group'),
        '#description' => $this->t('Enter the names of all originators of this matter ' .
            'including yourself. Separate them by comma. All names will be mentioned ' .
            'in the publicly visible licence information. <em>Required if the licence ' .
            'forces attribution by name, optional otherwise.</em>'),
        '#default_value' => Html::escape($this->decodeOrElse($item->origin, $name, 'group', null)),
    ];
    $container[$name]['check'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Statement'),
        '#description' => $this->t('Ich versichere, dass ich zu einer eine Gruppe ' .
            'von Menschen gehöre, die Urhebende dieses Werkes sind. Im Einverständnis ' .
            'mit der Gruppe wähle ich für die Veröffentlichung die nachstehende Lizenz.'),
        '#default_value' => false,
    ];
  }

  private function addOtherForm (array &$container, FieldItemInterface $item) {
    $name = 'other_originator';
    $this->addFieldset($container, $name, $this->t('Someone else is the originator'), 2);
    $container[$name]['originator'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Originator'),
        '#description' => $this->t('Enter the name of the originator of this matter. ' .
            '<em>Required if the licence forces attribution by name, optional otherwise.</em>'),
        '#default_value' => Html::escape($this->decodeOrElse($item->origin, $name, 'originator', null)),
    ];
    $container[$name]['check'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Statement'),
        '#description' => $this->t('Ich versichere, dass ich mich gewissenhaft ' .
            'über die Lizenzbedingungen dieses Werkes informiert habe und hier ' .
            'wahrheitsgemäß angebe, dass der Urheber die unten gewählte Lizenz zur ' .
            'Veröffentlichung erlaubt.'),
        '#default_value' => false,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues (array $values, array $form,
      FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['origin'] = json_encode($value['origin']);
    }
    return $values;
  }

}
