<?php

namespace Drupal\od_licence_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * A licence field.
 *
 * @FieldType(
 *   id = "od_licence_field",
 *   label = @Translation("Open Deutsch Licence Field"),
 *   description = @Translation("This field stores a licence item on the entity."),
 *   default_widget = "od_licence_widget",
 *   default_formatter = "od_licence_formatter",
 * )
 *
 * @author h2b
 */
class LicenceField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions (FieldStorageDefinitionInterface $field_definition) {
    $properties['origin'] = DataDefinition::create('string')->
      setLabel(t('Origin'))->
      setDescription(t('A string containing all originator data.'));
    $properties['licence'] = DataDefinition::create('string')->
        setLabel(t('Licence'))->
        setDescription(t('A string identifying the licence.'));
    $properties['source_url'] = DataDefinition::create('string')->
        setLabel(t('Source URL'))->
        setDescription(t('A string containing a link to the source of this matter.'));
    $properties['additional_notes'] = DataDefinition::create('string')->
        setLabel(t('Additional Licence Notes'))->
        setDescription(t('A string with additional notes.'));
    $properties['privacy_statement'] = DataDefinition::create('boolean')->
        setLabel(t('Privacy Statement'))->
        setDescription(t('A flag indicating privacy conderns are fulfilled.'));
    $properties['uid'] = DataDefinition::create('integer')->
        setLabel(t('User ID'))->
        setDescription(t('The ID of the user that attributed the licence.'))->
        setSetting('unsigned', TRUE);
    $properties['created'] = DataDefinition::create('timestamp')->
        setLabel(t('Created Time'))->
        setDescription(t('The time that the entry was created'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema (FieldStorageDefinitionInterface $field_definition) {
    $columns = [
        'origin' => [
            'description' => 'A plain text identifying the licence.',
            'type' => 'text',
            'size' => 'big',
        ],
        'licence' => [
            'description' => 'A plain text identifying the licence.',
            'type' => 'varchar',
            'length' => 255,
        ],
        'source_url' => [
            'description' => 'A string containing a link to the source of this matter.',
            'type' => 'text',
            'size' => 'normal',
        ],
        'additional_notes' => [
            'description' => 'A string with additional notes.',
            'type' => 'text',
            'size' => 'normal',
        ],
        'privacy_statement' => [
            'description' => 'A flag indicating privacy conderns are fulfilled.',
            'type' => 'int',
        ],
        'uid' => [
            'description' => 'The ID of the user that attributed the licence.',
            'type' => 'int',
            'unsigned' => TRUE,
        ],
        'created' => [
            'description' => 'A timestamp of when this entry has been created.',
            'type' => 'int',
        ],
    ];
    $schema = [
        'columns' => $columns,
        'indexes' => [],
        'foreign keys' => [],
    ];
    return $schema;
  }

}
