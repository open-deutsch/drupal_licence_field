<?php

namespace Drupal\od_licence_field;

use Drupal\Core\Url;

/**
 * Defines a map of Creative Commons licence objects.
 * @author h2b
 */
abstract class CreativeCommons {

  private static $licenceMap = null;
  private static $htmlMap = null;
  private static $linkMap = null;

  /**
   * Maps short name of the licence to the corresponding licence object.
   * @return array
   */
  public static function licenceMap () {
    if (self::$licenceMap === NULL) {
      $cc = [ self::cc0(), self::ccBy4(), self::ccBySa4() ];
      self::$licenceMap = [];
      foreach ($cc as $licence) self::$licenceMap[$licence->getShortName()] = $licence;
    }
    return self::$licenceMap;
  }

  /**
   * Maps short name of the licence to a proper HTML text.
   * @return array
   */
  public static function htmlMap () {
    if (self::$htmlMap === NULL) {
      self::$htmlMap = [];
      foreach (self::licenceMap() as $key => $licence)
        self::$htmlMap[$key] =
            "<strong><a href='{$licence->getUrl()->toString()}'>{$licence->getLongName()}.</a></strong> " .
            "<small>{$licence->getDescription()}</small>";
    }
    return self::$htmlMap;
  }

  /**
   * Maps short name of the licence to a proper HTML a tag.
   * @return array
   */
  public static function linkMap () {
    if (self::$linkMap === NULL) {
      self::$linkMap = [];
      foreach (self::licenceMap() as $key => $licence)
        self::$linkMap[$key] =
            "<a href='{$licence->getUrl()->toString()}'>{$licence->getShortName()}</a>";
    }
    return self::$linkMap;
  }

  private static function cc0 () {
    return new Licence('CC0 1.0',
        'CC0 1.0 Universell (CC0 1.0) Public Domain Dedication',
        'Die Freigabe des Inhalts unter der Creative Commons Zero-Lizenz (CC0) ' .
        'macht es möglich, Inhalte ohne Nachfrage zu beliebigen Zwecken weiter ' .
        'zu verwenden, insbesondere zu kopieren und zu veröffentlichen.',
        Url::fromUri('https://creativecommons.org/publicdomain/zero/1.0/deed.de'));
  }

  private static function ccBy4 () {
    return new Licence('CC BY 4.0',
        'Namensnennung 4.0 International (CC BY 4.0)',
        'Die Freigabe des Inhalts unter der Creative Commons Lizenz (CC BY 4.0) ' .
        'macht es möglich, Ihr Werk von anderen (weiter-)bearbeiten und wieder ' .
        'veröffentlichen zu lassen. Ihr Name als Urheber der ursprünglichen ' .
        'Fassung muss dabei immer genannt werden.',
        Url::fromUri('https://creativecommons.org/licenses/by/4.0/deed.de'));
  }

  private static function ccBySa4 () {
    return new Licence('CC BY-SA 4.0',
        'Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)',
        'Die Freigabe des Inhalts unter der Creative Commons Lizenz (CC0 BY SA 4.0) ' .
        'macht es möglich, Ihr Werk von anderen bearbeiten und wieder ' .
        'veröffentlichen zu lassen. Ihr Name als Urheber der ursprünglichen ' .
        'Fassung muss immer genannt werden. Das neu entstandene Werk oder auch ' .
        'die kommerzielle Nutzung Ihres Werkes muss unter den gleichen ' .
        'Lizenzbedingungen weitergegeben werden wie das Ausgangsdokument.',
        Url::fromUri('https://creativecommons.org/licenses/by-sa/4.0/'));
  }

}
