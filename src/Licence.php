<?php

namespace Drupal\od_licence_field;

use Drupal\Core\Url;

/**
 * A licence with common properties.
 * @author h2b
 */
class Licence {

  /**
   * The short name of this licence.
   *
   * @var string
   */
  protected $shortName;

  /**
   * The long name of this licence.
   *
   * @var string
   */
  protected $longName;

  /**
   * The description of this licence.
   *
   * @var string
   */
  protected $description;

  /**
   * The URL of this licence.
   *
   * @var Url
   */
  protected $url;

  /**
   * Constructs a new licence.
   * @param string $shortName
   * @param string $longName
   * @param string $description
   * @param Url $url
   */
  public function __construct(string $shortName, string $longName,
      string $description, Url $url) {
    $this->shortName = $shortName;
    $this->longName = $longName;
    $this->description = $description;
    $this->url = $url;
  }

  /**
   * @return string
   */
  public function getShortName ()
    {
      return $this->shortName;
  }

  /**
   * @return string
   */
  public function getLongName ()
    {
      return $this->longName;
  }

  /**
   * @return string
   */
  public function getDescription ()
    {
      return $this->description;
  }

  /**
   * @return \Drupal\Core\Url
   */
  public function getUrl ()
    {
      return $this->url;
  }

}
